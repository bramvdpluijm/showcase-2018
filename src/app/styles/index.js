import { injectGlobal } from 'styled-components';
import Lato from 'fonts/Lato/Lato-Regular.ttf';
import LatoBold from 'fonts/Lato/Lato-Bold.ttf';
import Merriweather from 'fonts/Merriweather/Merriweather-Regular.ttf';

export default () => injectGlobal`
    @font-face {
        font-family: 'Lato';
        src: url(${Lato});
    }

    @font-face {
        font-family: 'LatoBold';
        src: url(${LatoBold});
    }

    @font-face {
        font-family: 'Merriweather';
        src: url(${Merriweather});
    }

    html {
        box-sizing: border-box;
    }
    *, *:before, *:after {
        box-sizing: inherit;
    }

    body {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        font-family: 'Lato';
        background-color: ${__CLIENT__ && window.location.href.indexOf('/blog') > -1 ? '#f9f9f9' : '#171717'};
    }

    input, textarea {
        border: none;
        outline: none;
    }

    h1, h2, h3, h4, h5, h6 {
        font-family: 'Merriweather',sans-serif;
    }

    .ReactModal__Overlay {
        z-index: 10;
    }
`;
