let history;

if (__CLIENT__) {
    const createBrowserHistory = require('history/createBrowserHistory').default;

    history = createBrowserHistory();
}

export default history;
