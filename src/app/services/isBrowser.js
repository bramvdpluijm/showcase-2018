const isBrowser = () => (('window' in global) && ('document' in global));

export default isBrowser;
