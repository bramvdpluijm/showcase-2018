import qs from 'qs';
import history from 'services/history';
import API_ENDPOINT from 'config/api';

const request = ({ path, options, handle401, file }) => new Promise((resolve, reject) => {
    fetch(path, options)
        .then((response) => {
            if (response.ok) {
                if (file) return response.blob();
                return response.json();
            }

            return Promise.reject({ status: response.status, statusText: response.statusText });
        })
        .then((json) => {
            console.info('API call succeeded:', json);
            resolve(json);
        })
        .catch((error) => {
            console.error('API call failed:', error);
            reject(error);
        });
});

const generateOptions = ({ method, path, query, body, file = false }) => ({
    path: `${API_ENDPOINT}${path}${query ? '?' : ''}${qs.stringify(query || {})}`,
    options: {
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        method,
        ...(body ? { body: JSON.stringify(body) } : {}),
    },
    file,
});

export const get = ({ path, query, file }) =>
    request(generateOptions({ method: 'GET', path, query, file }));
export const del = ({ path, query, file }) =>
    request(generateOptions({ method: 'DELETE', path, query, file }));
export const post = ({ path, body, file }) =>
    request(generateOptions({ method: 'POST', path, body, file }));
export const put = ({ path, body, file }) =>
    request(generateOptions({ method: 'PUT', path, body, file }));
