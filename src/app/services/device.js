const size = {
    tablet: '768px',
    desktop: '1024px',
    desktopL: '1600px',
};

const device = {
    tablet: `(min-width: ${size.tablet})`,
    desktop: `(min-width: ${size.desktop})`,
    desktopL: `(min-width: ${size.desktopL})`,
};

export default device;
