const createUrlTitle = name => name.replace(/[\. ,:-]+/g, '-').toLowerCase();

export default createUrlTitle;
