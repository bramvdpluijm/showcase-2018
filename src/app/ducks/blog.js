import createAction from 'services/createAction';

const LOAD_BLOG_DATA = 'LOAD_BLOG_DATA';
const LOADED_BLOG_DATA = 'LOADED_BLOG_DATA';
const LOAD_BLOG_DATA_ERROR = 'LOAD_BLOG_DATA_ERROR';

const LOAD_CATEGORIES = 'LOAD_CATEGORIES';
const LOADED_CATEGORIES = 'LOADED_CATEGORIES';
const LOAD_CATEGORIES_ERROR = 'LOAD_CATEGORIES_ERROR';

const LOAD_BLOG_DETAIL = 'LOAD_BLOG_DETAIL';
const LOADED_BLOG_DETAIL = 'LOADED_BLOG_DETAIL';
const LOAD_BLOG_DETAIL_ERROR = 'LOAD_BLOG_DETAIL_ERROR';

const LOAD_USERS = 'LOAD_USERS';
const LOADED_USERS = 'LOADED_USERS';
const LOAD_USERS_ERROR = 'LOAD_USERS_ERROR';

const initialState = {
    blogData: [],
    categories: [],
    blogDetail: [],
    users: [],
    loadingBlogData: false,
    loadingBlogDataError: false,
    loadingCategories: false,
    loadCategoriesError: false,
    loadingBlogDetail: false,
    loadBlogDetailError: false,
    loadingUsers: false,
    loadUsersError: false,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
    case LOAD_BLOG_DATA:
        return {
            ...state,
            loadingBlogData: true,
        };
    case LOADED_BLOG_DATA:
        return {
            ...state,
            loadingBlogData: false,
            blogData: payload,
        };
    case LOAD_BLOG_DATA_ERROR:
        return {
            ...state,
            loadingBlogData: false,
            loadingBlogDataError: true,
            blogData: [],
        };
    case LOAD_CATEGORIES:
        return {
            ...state,
            loadingCategories: true,
        };
    case LOADED_CATEGORIES:
        return {
            ...state,
            loadingCategories: false,
            categories: payload,
        };
    case LOAD_CATEGORIES_ERROR:
        return {
            ...state,
            loadingCategories: false,
            loadCategoriesError: true,
            categories: [],
        };
    case LOAD_BLOG_DETAIL:
        return {
            ...state,
            loadingBlogDetail: true,
        };
    case LOADED_BLOG_DETAIL:
        return {
            ...state,
            loadingBlogDetail: false,
            blogDetail: payload,
        };
    case LOAD_BLOG_DETAIL_ERROR:
        return {
            ...state,
            loadingBlogDetail: false,
            loadBlogDetailError: true,
            blogDetail: [],
        };
    case LOAD_USERS:
        return {
            ...state,
            loadingUsers: true,
        };
    case LOADED_USERS:
        return {
            ...state,
            loadingUsers: false,
            users: payload,
        };
    case LOAD_USERS_ERROR:
        return {
            ...state,
            loadingUsers: false,
            loadUsersError: true,
            users: [],
        };
    default:
        return state;
    }
};

export const loadBlogData = createAction(LOAD_BLOG_DATA);
export const loadedBlogData = createAction(LOADED_BLOG_DATA);
export const loadBlogDataError = createAction(LOAD_BLOG_DATA_ERROR);

export const fetchBlogData = () => (dispatch, getState, api) => {
    dispatch(loadBlogData());

    return api.get({ path: '/posts' })
        .then((response) => {
            dispatch(loadedBlogData(response));
        })
        .catch((err) => {
            dispatch(loadBlogDataError(err));
        });
};

export const loadCategories = createAction(LOAD_CATEGORIES);
export const loadedCategories = createAction(LOADED_CATEGORIES);
export const loadCategoriesError = createAction(LOAD_CATEGORIES_ERROR);

export const fetchCategories = () => (dispatch, getState, api) => {
    dispatch(loadCategories());

    return api.get({ path: '/categories' })
        .then((response) => {
            dispatch(loadedCategories(response));
        })
        .catch((err) => {
            dispatch(loadCategoriesError(err));
        });
};

export const loadBlogDetail = createAction(LOAD_BLOG_DETAIL);
export const loadedBlogDetail = createAction(LOADED_BLOG_DETAIL);
export const loadBlogDetailError = createAction(LOAD_BLOG_DETAIL_ERROR);

export const fetchBlogDetail = postSlug => (dispatch, getState, api) => {
    dispatch(loadBlogDetail());

    return api.get({ path: `/posts?slug=${postSlug}` })
        .then((response) => {
            dispatch(loadedBlogDetail(response));
        })
        .catch((err) => {
            dispatch(loadBlogDetailError(err));
        });
};

export const loadUsers = createAction(LOAD_USERS);
export const loadedUsers = createAction(LOADED_USERS);
export const loadUsersError = createAction(LOAD_USERS_ERROR);

export const fetchUsers = () => (dispatch, getState, api) => {
    dispatch(loadUsers());

    return api.get({ path: '/users' })
        .then((response) => {
            dispatch(loadedUsers(response));
        })
        .catch((err) => {
            dispatch(loadUsersError(err));
        });
};
