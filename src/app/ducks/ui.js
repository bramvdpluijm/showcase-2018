import createAction from 'services/createAction';

const SET_SLIDE_INDEX = 'SET_SLIDE_INDEX';
// const SET_PREVIOUS_PAGE = 'SET_PREVIOUS_PAGE';

const initialState = {
    slideIndex: 0,
    // previousPage: '',
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
    case SET_SLIDE_INDEX:
        return {
            ...state,
            slideIndex: payload,
        };
    // case SET_PREVIOUS_PAGE:
    //     return {
    //         ...state,
    //         previousPage: payload,
    //     };
    default:
        return state;
    }
};

export const setSlideIndex = createAction(SET_SLIDE_INDEX);
// export const setPreviousPage = createAction(SET_PREVIOUS_PAGE);
