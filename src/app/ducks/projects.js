import createAction from 'services/createAction';
import createUrlTitle from 'services/createUrlTitle';
import Database from 'data/database.js';

const LOADED_PROJECT_DETAIL = 'LOADED_PROJECT_DETAIL';

const initialState = { projectDetails: [] };

export default (state = initialState, { type, payload }) => {
    switch (type) {
    case LOADED_PROJECT_DETAIL:
        return {
            ...state,
            projectDetails: payload,
        };
    default:
        return state;
    }
};

export const loadedProjectDetail = createAction(LOADED_PROJECT_DETAIL);

export const fetchProjectDetail = projectName => (dispatch) => {
    dispatch(loadedProjectDetail(Database.projects.filter(project => (projectName === createUrlTitle(project.project_name)))));
};
