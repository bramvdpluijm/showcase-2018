import NatuurlijkPresteren from 'images/natuurlijkpresteren.jpg';
import Huisblusser from 'images/huisblusser.jpg';
import ApolloHotels from 'images/apollo.jpg';
import NockNock from 'images/nocknock.jpg';
// import VcnPortal from 'images/vcn-portal.jpg';
import VcnWebsite from 'images/vcnwebsite.jpg';
import Thermae2000 from 'images/thermae.jpg';

function importAll(r) {
    return r.keys().map(r);
}

const natuurlijkScreencaps = importAll(require.context('images/screencaptures/natuurlijkpresteren', false, /\.(png|jpe?g)$/));
const apolloScreencaps = importAll(require.context('images/screencaptures/apollohotels', false, /\.(png|jpe?g)$/));
const vcnWebsiteScreencaps = importAll(require.context('images/screencaptures/VCN-website', false, /\.(png|jpe?g)$/));
const huisblusserScreencaps = importAll(require.context('images/screencaptures/huisblusser', false, /\.(png|jpe?g)$/));
const thermaeScreencaps = importAll(require.context('images/screencaptures/thermae', false, /\.(png|jpe?g)$/));

/* eslint-disable */ // for the sake of json readability.
export default {
  "about": {
    "title": "Hello,",
    "subtitle": "is it me you're looking for?",
    "paragraphs": [
      "Ik ben Bram, een creatieve Front-end Developer van 23 jaar. In mijn vrije tijd hou ik ervan om dingen te maken. 'Dingen' is een breed begrip wat op dit moment voor mij betekent: experimenteren met nieuwe web technieken en het zelf brouwen van het perfecte biertje (en die vervolgens ook opdrinken natuurlijk).",
      "Qua werk haal ik de meeste voldoening uit het werken met een ijzersterk team, het maken van een solide product en het schrijven van goed getestte, leesbare code. Samen toffe dingen bouwen en doelen halen! Wanneer ik aan het einde van de dag een complex probleem heb opgelost kan ik met een gerust hart naar huis.",
      "Ondertussen heb ik 4 jaar ervaring als Front-end Developer. Deze ervaring heb ik voornamelijk opgedaan bij Label A in Amsterdam waar ik jaren met veel plezier en een fantastisch team gewerkt heb aan verschillende complexe (langlopende)projecten. Ook met het Freelancen voor eigen klanten heb ik de nodige ervaring opgedaan. Op dit moment sta ik open voor een nieuwe uitdaging!"
    ]
  },
  "projects": [
    {
      "project_name": "Natuurlijk Presteren",
      "project_lines": [
        "Natuurlijk",
        "Presteren"
      ],
      "project_alineas": [
        {
          "text": "Natuurlijk Presteren is een webshop die natuurlijke supplementen verkoopt. Het is begonnen met de verkoop van concentratiecapsules voor - in eerste instantie - studenten. Later is de eigenaar van dit bedrijf zich gaan richten op uitbreiding en verkoop van meer supplementen dan alleen de concentratiecapsules."
        },
        {
            "text": "Met het oog op uitbreiding is er contact met mij opgenomen om een website/webshop te bouwen. Dit heb ik op freelance basis gedaan in een aantal weken tijd. Wat deze site bijzonder maakt is dat ik zelf ook de vormgeving heb gedaan. Natuurlijkpresteren.nl was mijn eerste freelance wordpress project waar ik zelf een thema voor ontwikkeld heb."
        }
      ],
      "project_image": NatuurlijkPresteren,
      "project_average_color": "rgb(213, 220, 224)",
      "skills": [
        "Responsive Design",
        "UX",
        "Backend Development",
        "Wordpress CMS Integration",
        "Frontend Development",
      ],
      "project_url": "https://www.natuurlijkpresteren.nl",
      "project_screencaps": natuurlijkScreencaps,
      "project_keywords": "natuurlijk presteren, concentratiecapsules, concentratie, supplementen, studenten, webshop, website, wordpress, wordpress thema, thema",
    },
    {
        "project_name": "Verscentrum Nederland",
        "project_lines": [
            "Verscentrum",
            "Nederland"
        ],
        "project_alineas": [
            {
              "text": "Verscentrum Nederland is de totaalleverancier voor de zorg-, bedrijven en retailsector. Vanuit hun distributiecentra leveren zij door het hele land een compleet aanbod aan producten. Zij bieden voor elke sector passende concepten naar wens van de klant. Op dit moment beschikt VCN over 3 distributiecentra met ruim 35.000 artikelen en vanuit die centra leveren zij 6 dagen per week producten aan ruim 4.000 klanten in Nederland."
            },
            {
                "text": "VCN heeft Label A in de arm genomen om hen digitaal naar een hoger niveau te tillen. We hebben een complete roadmap opgesteld om ervoor te zorgen dat we alle digitale middelen gecovered hebben. Een van de onderdelen op de roadmap was de corporate website; een overkoepelende, informatieve website over de organisatie."
            },
            {
                "text": "Een ander - later - onderdeel van de roadmap was een systeem voor de klantenservice om ervoor te zorgen dat alle telefoontjes van klanten gedocumenteerd konden worden. Over dat systeem mag ik hier inhoudelijk helaas niets schrijven (non-disclosure agreement), maar ik ben er wel super trots op. Dit project is namelijk het langstlopende React project geweest wat ik heb gedraaid en waar ik het meeste van heb geleerd. In het begin heb ik dit project samen gedraaid met een collega en in een later stadium alleen. Een hele mooie ervaring voor mij als Developer en een super toevoeging voor het VCN team."
            }
        ],
        project_image: VcnWebsite,
        "project_average_color": "rgb(163, 149, 143)",
        "skills": [
          "Wagtail CMS Integration",
          "Frontend Development",
          "User Testing",
          "Redux",
          "React",
          "Single Sign On",
          "Identity provider Integration",
          "Rest api integration"
        ],
        "project_url": "https://www.verscentrumnederland.nl",
        "project_screencaps": vcnWebsiteScreencaps,
        "project_keywords": "verscentrum nederland, vcn, producten, corporate website, website, informatief, react, redux, developer, scrum, agile, user testing, testing, wagtail, frontend",
    },
    {
        "project_name": "Apollo Hotels",
        "project_lines": [
          "Apollo",
          "Hotels"
        ],
        "project_alineas": [
          {
            "text": "Bij Apollo Hotels beleeft de gast een unieke hotelovernachting. Met 24/7 ontbijt en een persoonlijke “Call Jenny” hotline vanuit je hotelkamer voor het geval je iets nodig hebt, wordt vooral ingespeeld op de beleving. Daar hoort natuurlijk ook een unieke website bij."
          },
          {
            "text": "Om deze beleving ook hier te realiseren, ontwikkelden we een volledig nieuwe site. De focus lag vooral op het optimaliseren van de user experience en het versoepelen van het boekingsproces. Een koppeling met TravelClick geeft de gasten direct inzicht in het aantal en type beschikbare kamers. Nog belangrijker: het geeft realtime inzicht in de actuele prijzen. Het integreren van Tripadvisor reviews in de website was een gewaagde stap, maar houdt Apollo Hotels juist scherp. Slechte reviews op je eigen site zijn natuurlijk niet zo lekker. De ingebouwde Instagram feed maakt de site nog dynamischer en zorg voor een next level beleving."
          },
          {
            "text": "Doordat het boekingsproces in de website is geïntegreerd hebben we de conversie ratio flink kunnen verhogen."
          },
          {
            "text": "Deze site heb ik gebouwd toen ik in dienst was bij Label A, samen met nog ongeveer 2 front-enders. Ik heb me voornamelijk bezig gehouden met het integreren van de boekingsmodule en het stylen van de verschillende pages."
          }
        ],
        "project_image": ApolloHotels,
        "project_average_color": "rgb(51, 61, 69)",
        "skills": [
          "Mura CMS Integration",
          "Frontend Development",
          "Created Booking Bar",
        ],
        "project_url": "https://www.apollohotels.nl",
        "project_screencaps": apolloScreencaps,
        "project_keywords": "mura cms, mura, coldfusion, booking bar, booking, boekingsmodule, conversie, user experience",
      },
    {
      "project_name": "Huisblusser.nl",
      "project_lines": [
        "Huisblusser.nl"
      ],
      "project_alineas": [
        {
          "text": "Huisblusser.nl is een bedrijf dat online brandblussers wil verkopen aan particulieren. Deze blussers zijn allemaal voorzien van een keurmerk. Op dit moment heeft de ondernemer al een REOB- gecertificeerd brandbeveiligingsbedrijf dat zich richt op goede service aan huis. Met huisblusser.nl kunnen gekeurde blussers worden verkocht tegen een lagere prijs omdat er minder service mee verkocht wordt."
        },
        {
            "text": "Voor huisblusser.nl heb ik een custom wordpress thema ontwikkeld. Tevens heb ik in dit project voor het eerst zelf een DigitalOcean droplet ingesteld om ervoor te zorgen dat de klant vlot door de webshop en website kan navigeren. Hiermee heb ik veel geleerd over DNS, SSL, Caching en het werken op een Ubuntu server."
        }
      ],
      "project_image": Huisblusser,
      "project_average_color": "rgb(218, 96, 24)",
      "skills": [
        "Responsive Design",
        "UX",
        "Backend Development",
        "DevOps",
        "Wordpress CMS Integration",
        "Frontend Development",
      ],
      "project_screencaps": huisblusserScreencaps,
      "project_keywords": "huisblusser, brandblusser, huisblusser.nl, webshop, website, responsive design, responsive, devops, ubuntu, backend, frontend, digitalocean, dns, ssl, caching, brandbeveiligingsbedrijf, brandbeveiliging",
    },
    {
        "project_name": "Thermae 2000",
        "project_lines": [
          "Thermae",
          "2000"
        ],
        "project_alineas": [
          {
            "text": "Thermae 2000 is een wellness center gevestigd in het Limburgse Valkeburg aan de Geul. Het water van Thermae 2000 wordt gepompt uit 381 meter diepte en is afkomstig uit een onderaards meer. Voor Thermae 2000 heb ik samen met mijn team een nieuwe website gerealiseerd alsmede een nieuw bookingsysteem (in latere sprints)."
          },
          {
            "text": "Thermae is een speciaal project. Dat komt omdat het bestaat uit een coldfusion voorkant en voor het meer complexe gedeelte (de boekingflow) een React applicatie. Wat je op de foto hierboven kunt zien is dat er aan de rechterkant een witte module aanwezig is. Die module is volledig React. Op het moment dat je daar een aantal data invoert, word je met die ingevulde data de boekingsmodule in gestuurd. De boekingflow zelf is volledig React. Daar kan een gebruiker de beschikbaarheid van behandelingen checken en deze meteen boeken. Ook kan er gekozen worden om meteen extra's toe te voegen per gast."
          }
        ],
        project_image: Thermae2000,
        "project_average_color": "rgba(26, 180, 207, 0.9)",
        "skills": [
          "Mura CMS Integration",
          "Frontend Development",
          "React",
          "Redux",
          "Coldfusion"
        ],
        "project_url": "https://www.thermae.nl",
        "project_screencaps": thermaeScreencaps,
        "project_keywords": "thermae 2000, thermae, thermaal, thermale baden, valkenburg, wellness, website, bookingsysteem, mura cms, mura, react, redux, boekingsmodule, behandelingen",
    },
    {
      "project_name": "Nocknock",
      "project_lines": [
        "Nocknock"
      ],
      "project_alineas": [
        {
          "text": "Nocknock startte als een concept. Niet altijd kunnen pakketjes de eerste keer dat de bezorger langskomt bezorgd worden doordat er niemand thuis is. Het afleveren bij de buren is vaak ook geen succes. Daarom is Nocknock in het leven geroepen. Met Nocknock krijgt de gebruiker een speciaal slot geinstalleerd zodat de bezorger met een speciale gemachtigde app de deur open kan doen om het pakket binnen te leggen. De eigenaar van het huis heeft ook een app waarin te zien is wanneer de bezorger bij hem thuis is en of het pakket al is bezorgd."
        },
        {
          "text": "Voor Nocknock heb ik een one pager gerealiseerd met interactieve elementen. Zo heb ik een infographic gemaakt met Greensock (animatie library), die gaat animeren op het moment dat dat gedeelte van de one pager in beeld is. Op de infographic is de oude situatie te zien en ook de situatie met Nocknock."
        },
        {
            "text": "Deze one-pager heb ik in twee weken gebouwd bij Label A. Klant was erg tevreden en heeft hier campagne mee gevoerd om funding te krijgen. Doordat de ondernemer achter dit concept niet door is gegaan met het product, is de website niet langer in de lucht."
        }
      ],
      project_image: NockNock,
      "project_average_color": "rgb(40, 174, 216)",
      "skills": [
        "Frontend Development",
        "Greensock Animations",
      ],
      "project_keywords": "nocknock, pakketjes, pakketten, bezorger, afleveren, one pager, interactieve elementen, infographic, greensock, Label A, label a",
    }
  ]
}
