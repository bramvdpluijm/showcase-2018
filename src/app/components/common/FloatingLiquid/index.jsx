import React, { Component } from 'react';
import styled from 'styled-components';
import device from 'services/device';
import threeEntryPoint from './components/threeEntryPoint';

const FloaterContainer = styled.div`
    position: fixed;
    bottom: -200px;
    left: -400px;
    @media ${device.desktop} {
        left: -200px;
    }
`;

class FloatingLiquid extends Component {
    constructor() {
        super();

        this.state = {
            stopRendering: false,
        };
    }

    componentDidMount() {
        threeEntryPoint(this.threeRootElement);
    }

    componentWillUnmount() {
        if (__CLIENT__) {
            window.cancelAnimationFrame(window.renderAnimationFrame);
        }
        this.setState({ stopRendering: true });
    }

    render() {
        const { stopRendering } = this.state;
        return (
            !stopRendering && (
                <FloaterContainer>
                    <div ref={element => this.threeRootElement = element} />
                </FloaterContainer>
            )
        );
    }
}

export default FloatingLiquid;
