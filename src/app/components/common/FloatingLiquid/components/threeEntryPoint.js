import SceneManager from './sceneManager';

export default (container) => {
    function createCanvas(document, container) {
        const newCanvas = document.createElement('canvas');
        newCanvas.width = 800;
        newCanvas.height = 800;
        container.appendChild(newCanvas);
        return newCanvas;
    }

    const canvas = createCanvas(document, container);
    const sceneManager = new SceneManager(canvas);

    let canvasHalfWidth;
    let canvasHalfHeight;

    // function resizeCanvas() {
    //     sceneManager.onWindowResize();
    // }

    function mouseMove({ screenX, screenY }) {
        sceneManager.mouseMove(screenX - canvasHalfWidth, screenY - canvasHalfHeight);
    }

    function bindEventListeners() {
        window.onmousemove = mouseMove;
    }

    function render(a) {
        window.renderAnimationFrame = requestAnimationFrame(render);
        sceneManager.update(a);
    }

    bindEventListeners();
    render();
};
