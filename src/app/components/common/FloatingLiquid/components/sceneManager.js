import * as THREE from 'three';
import { Noise } from 'noisejs';
import { tween, easing } from 'popmotion';

export default (canvas) => {
    const noise = new Noise(Math.random());
    let width = canvas.offsetWidth;
    let height = canvas.offsetHeight;

    const mouse = new THREE.Vector2(0.8, 0.5);

    const scene = new THREE.Scene();

    function buildRender() {
        const renderer = new THREE.WebGLRenderer({
            canvas,
            antialias: true,
        });
        renderer.setPixelRatio(window.devicePixelRatio > 1 ? 2 : 1);
        renderer.setSize(width, height);
        renderer.setClearColor(0x171717);
        return renderer;
    }

    function buildCamera() {
        const camera = new THREE.PerspectiveCamera(100, width / height, 0.1, 10000);
        camera.position.set(0, 0, 180);

        return camera;
    }

    const light = new THREE.HemisphereLight(0x000000, 0x000000, 0.7);
    scene.add(light);

    const light2 = new THREE.DirectionalLight(0xffffff, 0.3);
    light2.position.set(200, 300, 400);
    scene.add(light2);
    const light3 = light2.clone();
    light3.position.set(-200, 300, 400);
    scene.add(light3);

    const geometry = new THREE.IcosahedronGeometry(150, 4);
    for (let i = 0; i < geometry.vertices.length; i++) {
        const vector = geometry.vertices[i];
        vector.o = vector.clone();
    }

    const material = new THREE.MeshPhongMaterial({
        emissive: 0xffffff,
        emissiveIntensity: 0,
        shininess: 0,
    });
    const shape = new THREE.Mesh(geometry, material);
    scene.add(shape);

    function updateVertices(a) {
        for (let i = 0; i < geometry.vertices.length; i++) {
            const vector = geometry.vertices[i];
            vector.copy(vector.o);
            const perlin = noise.simplex3(
                (vector.x * 0.006) + (a * 0.0002),
                (vector.y * 0.006) + (a * 0.0003),
                (vector.z * 0.006),
            );
            const ratio = ((perlin * 0.4 * (mouse.y + 0.1)) + 0.8);
            vector.multiplyScalar(ratio);
        }
        geometry.verticesNeedUpdate = true;
    }

    function onMouseMove(e) {
        tween(mouse, 0.8, {
            from: { y: e.clientY / height, x: e.clientX / width },
            to: 10, // Both x and y will tween to 0
            ease: { x: easing.easeOut, y: easing.easeIn }
        });
    }

    const camera = buildCamera();
    const renderer = buildRender();

    // function onResize() {
    //     canvas.style.width = '';
    //     canvas.style.height = '';
    //     width = canvas.offsetWidth;
    //     height = canvas.offsetHeight;
    //     camera.aspect = width / height;
    //     camera.updateProjectionMatrix();
    //     renderer.setSize(width, height);
    // }

    // let resizeTm;
    // function onWindowResize() {
    //     resizeTm = clearTimeout(resizeTm);
    // }

    const mouseMove = e => onMouseMove(e);

    function update(a) {
        updateVertices(a);
        renderer.render(scene, camera);
    }

    return {
        mouseMove,
        update,
    };
};
