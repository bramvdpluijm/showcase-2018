import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import styled from 'styled-components';
import posed from 'react-pose';
import { tween, easing } from 'popmotion';
import PT from 'prop-types';

const AnimatedRouteElement = styled.div`
    height: 100vh;
    background-color: #171717;
`;

const windowHeight = __CLIENT__ ? window.innerHeight : 0;

const AnimatedSlideTop = styled(posed.div({
    open: {
        y: '-100%',
        scaleY: 1,
        transition: props => tween({
            ...props,
            duration: 600,
            ease: easing.easeIn,
        }),
    },
    closed: {
        y: '0%',
        scaleY: 0,
    },
}))`
    position: fixed;
    width: 100%;
    top: 0;
    height: ${windowHeight / 2}px;
    background-color: #fff;
    transform-origin: bottom center;
    z-index: 5;
`;

const AnimatedSlideBottom = styled(posed.div({
    open: {
        y: '100%',
        scaleY: 1,
        transition: props => tween({
            ...props,
            duration: 600,
            ease: easing.easeIn,
        }),
    },
    closed: {
        y: '0%',
        scaleY: 0,
    },
}))`
    position: fixed;
    width: 100%;
    top: ${windowHeight / 2}px;
    height: ${windowHeight / 2}px;
    background-color: #fff;
    transform-origin: top center;
    z-index: 5;
`;

const BarsContainer = styled(posed.div({
    open: {
        delayChildren: 250,
        staggerChildren: 100,
        opacity: 1,
    },
    closed: {
        opacity: 0,
    },
}))`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: flex;
    width: 200px;
    justify-content: space-between;
    z-index: 15;
`;

const LoadingBar = styled(posed.div({
    open: {
        y: '50px',
        transition: props => tween({
            ...props,
            duration: 1000,
            ease: easing.backOut,
            flip: Infinity,
        }),
    },
    closed: {
        y: '0px',
        transition: props => tween({
            ...props,
            duration: 1000,
            ease: easing.backOut,
            flip: Infinity,
        }),
    },
}))`
    width: 15px;
    height: 40px;
    background-color: #fff;
    z-index: 10;
`;

const PageContent = posed.div({
    open: {
        opacity: 1,
        transition: props => tween({
            ...props,
            duration: 300,
            ease: easing.easeIn,
        }),
    },
    closed: {
        opacity: 0,
        transition: props => tween({
            ...props,
            duration: 300,
            ease: easing.easeIn,
        }),
    },
});

class AnimatedRoute extends React.Component {
    constructor() {
        super();
        this.state = {
            startEnterAnimation: false,
            startBarsAnimation: false,
            showPageContent: false,
        };
    }

    componentDidMount() {
        // Timing the animations
        this.enterAnimation = setTimeout(() => {
            this.setState({ startEnterAnimation: true });
        }, 1500);

        this.startBarsAnimation = setTimeout(() => {
            this.setState({ startBarsAnimation: true });
        }, 0);

        this.removeBarsAnimation = setTimeout(() => {
            this.setState({ startBarsAnimation: false });
        }, 1300);

        this.showPageContent = setTimeout(() => {
            this.setState({ showPageContent: true });
        }, 2000);
    }

    componentWillUnmount() {
        clearTimeout(this.enterAnimation);
        clearTimeout(this.startBarsAnimation);
        clearTimeout(this.removeBarsAnimation);
        clearTimeout(this.showPageContent);
    }

    render() {
        const { path, component, exact } = this.props;

        return (
            <AnimatedRouteElement>
                <Fragment>
                    <AnimatedSlideTop pose={this.state.startEnterAnimation ? 'open' : 'closed'} />
                    <BarsContainer pose={this.state.startBarsAnimation ? 'open' : 'closed'}>
                        <LoadingBar />
                        <LoadingBar />
                        <LoadingBar />
                        <LoadingBar />
                        <LoadingBar />
                        <LoadingBar />
                        <LoadingBar />
                        <LoadingBar />
                    </BarsContainer>
                    <AnimatedSlideBottom pose={this.state.startEnterAnimation ? 'open' : 'closed'} />
                </Fragment>
                <PageContent pose={this.state.showPageContent ? 'open' : 'closed'}>
                    <Route path={path} component={component} exact={exact} />
                </PageContent>
            </AnimatedRouteElement>
        );
    }
}

AnimatedRoute.propTypes = {
    path: PT.string,
    component: PT.func,
    exact: PT.bool,
};

export default AnimatedRoute;
