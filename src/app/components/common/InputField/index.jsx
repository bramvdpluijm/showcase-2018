import React from 'react';
import PT from 'prop-types';
import styled from 'styled-components';

const Input = styled.input`
    padding: 10px;
    width: 100%;
    box-sizing: border-box;
    border-radius: 4px;
`;

const TextArea = styled.textarea`
    padding: 10px;
    width: 100%;
    box-sizing: border-box;
    min-height: 200px;
    border-radius: 4px;
`;

const InputField = ({ input, label, type, meta: { touched, error, warning, active }, textarea, placeholder }) => (
    textarea ? (
        <TextArea id={input.name} {...input} value={input.value} placeholder={placeholder} />
    ) : (
        <Input type={type} id={input.name} {...input} value={input.value} placeholder={placeholder} autoComplete={input.name} />
    )
);

InputField.propTypes = {
    input: PT.object,
    label: PT.string,
    type: PT.string,
    textarea: PT.bool,
    placeholder: PT.string,
};

export default InputField;
