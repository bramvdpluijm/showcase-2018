import styled from 'styled-components';
import posed from 'react-pose';

import device from 'services/device';

export const MenuHolder = styled.div`
    position: absolute;
    top: 55px;
    right: 30px;
    width: 50px;
    cursor: pointer;
    z-index: 5;
    span {
        display: block;
        float: right;
        width: 100%;
        height: 1px;
        background-color: ${props => props.black ? '#000000' : '#ffffff'};
        margin-top: 4px;
        clear: both;
        transition: all 150ms ease-out;

        &:nth-of-type(2) {
            width: 35.38%;
        }

        &:nth-of-type(3) {
            width: 81.53%;
        }

        &:nth-of-type(4) {
            width: 63.07%;
        }
    }

    &:hover {
        span {
            width: 100%;
        }
    }
`;

export const MenuTopbar = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    z-index: 1;
    svg {
        width: auto;
        height: 70px;
    }
`;

export const MenuContent = styled.nav`
    display: block;
    width: 100%;
    z-index: 5;
`;

export const ListContainer = styled(posed.ul({
    open: {
        delayChildren: 250,
        staggerChildren: 100,
    },
}))`
    margin: 0;
    padding: 0;
    list-style: none;
    text-align: center;
    @media ${device.desktop} {
        text-align: right;
    }
`;

export const ListItems = styled(posed.li({
    open: { y: 0, opacity: 1 },
    closed: { y: 20, opacity: 0 },
}))`
    border-top: 1px solid #ccc;
    &:last-of-type {
        border-bottom: 1px solid #ccc;
    }
    a {
        position: relative;
        text-decoration: none;
        color: #fff;
        font-family: 'Lato';
        font-size: 35px;
        line-height: 60px;
        letter-spacing: 4px;
        @media ${device.desktop} {
            font-size: 10vh;
            line-height: 11vh;
        }
    }
    @media ${device.desktop} {
        border: none;
        &:last-of-type {
            border: none;
        }
    }
`;

export const MenuFooter = styled.div`
    display: block;
    width: 100%;
    text-align: right;
    a {
        color: #fff;
        font-size: 25px;
        text-decoration: none;
        position: relative;
        padding-bottom: 10px;
        &:after {
            content: '';
            width: 0%;
            height: 2px;
            background-color: #fff;
            position: absolute;
            bottom: 5px;
            left: 0;
            transition: width 0.3s ease-out;
        }
        &:hover {
            &:after {
                content: '';
                width: 100%;
                transition: width 0.3s ease-out;
            }
        }
    }
`;

export const CloseIcon = styled.div`
    cursor: pointer;
    width: 50px;
    height: 50px;
    transform: matrix(1, 0, 0, 1, 0, 0);
    span {
        position: absolute;
        top: 50%;
        left: 50%;
        height: 1px;
        width: 100%;
        margin-left: -25px;
        background-color: #fff;
        transition: all 100ms ease-out;
        &:nth-of-type(1) {
            transform: rotate(-45deg);
        }
        &:nth-of-type(2) {
            transform: rotate(45deg);
        }
    }
    &:hover {
        span {
            transform: none;
        }
    }
`;
