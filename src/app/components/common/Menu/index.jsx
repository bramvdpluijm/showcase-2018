import React, { Fragment } from 'react';
import Modal from 'react-modal';
import { Link } from 'react-router-dom';
import Logo from 'vectors/bram-logo-white.svg';
import FloatingLiquid from 'common/FloatingLiquid';
import { MenuHolder, MenuTopbar, CloseIcon, MenuContent, MenuFooter, ListContainer, ListItems } from './styled';

const modalStyles = {
    content: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'column',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        border: 'none',
        borderRadius: 0,
        padding: 30,
        backgroundColor: '#171717',
    },
};

class Menu extends React.Component {
    constructor() {
        super();

        this.state = {
            menuOpen: false,
            showMenuAnimation: false,
        };
    }

    componentWillMount() {
        Modal.setAppElement('body');
        this.setState({ showMenuAnimation: false });
    }

    componentDidUpdate() {
        if (this.state.menuOpen) {
            setTimeout(() => {
                this.setState({ showMenuAnimation: true });
            }, 100);
        }

        if (this.state.showMenuAnimation && !this.state.menuOpen) {
            this.setState({ showMenuAnimation: false });
        }
    }

    render() {
        const { black } = this.props;
        return (
            <Fragment>
                <MenuHolder black={black} onClick={() => this.setState({ menuOpen: true })}>
                    <span />
                    <span />
                    <span />
                    <span />
                </MenuHolder>
                {__CLIENT__ && (
                    <Modal
                        isOpen={this.state.menuOpen}
                        onRequestClose={() => this.setState({ menuOpen: false })}
                        contentLabel="Menu"
                        style={modalStyles}
                    >
                        <MenuTopbar>
                            <Logo />
                            <CloseIcon onClick={() => this.setState({ menuOpen: false })}>
                                <span />
                                <span />
                            </CloseIcon>
                        </MenuTopbar>
                        <MenuContent>
                            <ListContainer pose={this.state.showMenuAnimation ? 'open' : 'closed'}>
                                <ListItems onClick={() => this.setState({ menuOpen: false })}>
                                    <Link to="/over-mij">Over mij</Link>
                                </ListItems>
                                <ListItems onClick={() => this.setState({ menuOpen: false })}>
                                    <Link to="/projecten">Projecten</Link>
                                </ListItems>
                                <ListItems onClick={() => this.setState({ menuOpen: false })}>
                                    <Link to="/blog">Blog</Link>
                                </ListItems>
                            </ListContainer>
                        </MenuContent>
                        <MenuFooter>
                            <FloatingLiquid />
                            <a href="mailto:hello@pluijm.io">hello@pluijm.io</a>
                        </MenuFooter>
                    </Modal>
                )}
            </Fragment>
        );
    }
}

export default Menu;
