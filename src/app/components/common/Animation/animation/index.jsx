import { keyframes } from 'styled-components';

export const Glitch1 = keyframes`
    0% {
        transform: none;
        opacity: 1;
    }
    7% {
        transform: skew(${Math.floor(Math.random() * 1) + 0}deg, -${Math.floor(Math.random() * 1) + 0}deg);
        opacity: 0.75;
    }
    10% {
        transform: none;
        opacity: 1;
    }
    27% {
        transform: none;
        opacity: 1;
    }
    30% {
        transform: skew(${Math.floor(Math.random() * 1) + 0}deg, -${Math.floor(Math.random() * 1) + 0}deg);
        opacity: 0.75;
    }
    35% {
        transform: none;
        opacity: 1;
    }
    52% {
        transform: none;
        opacity: 1;
    }
    55% {
        transform: skew(-${Math.floor(Math.random() * 1) + 0}deg, ${Math.floor(Math.random() * 1) + 0}deg);
        opacity: 0.75;
    }
    50% {
        transform: none;
        opacity: 1;
    }
    72% {
        transform: none;
        opacity: 1;
    }
    75% {
        transform: skew(${Math.floor(Math.random() * 1) + 0}deg, ${Math.floor(Math.random() * 1) + 0}deg);
        opacity: 0.75;
    }
    80% {
        transform: none;
        opacity: 1;
    }
    100% {
        transform: none;
        opacity: 1;
    }
`;

export const Glitch2 = keyframes`
    0% {
        transform: none;
        opacity: 0.25;
    }
    7% {
        transform: translate(-${Math.floor(Math.random() * 6) + 1}px, -${Math.floor(Math.random() * 6) + 1}px);
        opacity: 0.5;
    }
    10% {
        transform: none;
        opacity: 0.25;
    }
    27% {
        transform: none;
        opacity: 0.25;
    }
    30% {
        transform: translate(-${Math.floor(Math.random() * 6) + 1}px, -${Math.floor(Math.random() * 6) + 1}px);
        opacity: 0.5;
    }
    35% {
        transform: none;
        opacity: 0.25;
    }
    52% {
        transform: none;
        opacity: 0.25;
    }
    55% {
        transform: translate(-${Math.floor(Math.random() * 6) + 1}px, -${Math.floor(Math.random() * 6) + 1}px);
        opacity: 0.5;
    }
    50% {
        transform: none;
        opacity: 0.25;
    }
    72% {
        transform: none;
        opacity: 0.25;
    }
    75% {
        transform: translate(-${Math.floor(Math.random() * 6) + 1}px, -${Math.floor(Math.random() * 6) + 1}px);
        opacity: 0.5;
    }
    80% {
        transform: none;
        opacity: 0.25;
    }
    100% {
        transform: none;
        opacity: 0.25;
    }
`;
export const Glitch3 = keyframes`
    0% {
        transform: none;
        opacity: 0.25;
    }
    7% {
        transform: translate(${Math.floor(Math.random() * 6) + 1}px, ${Math.floor(Math.random() * 6) + 1}px);
        opacity: 0.5;
    }
    10% {
        transform: none;
        opacity: 0.25;
    }
    27% {
        transform: none;
        opacity: 0.25;
    }
    30% {
        transform: translate(${Math.floor(Math.random() * 6) + 1}px, ${Math.floor(Math.random() * 6) + 1}px);
        opacity: 0.5;
    }
    35% {
        transform: none;
        opacity: 0.25;
    }
    52% {
        transform: none;
        opacity: 0.25;
    }
    55% {
        transform: translate(${Math.floor(Math.random() * 6) + 1}px, ${Math.floor(Math.random() * 6) + 1}px);
        opacity: 0.5;
    }
    50% {
        transform: none;
        opacity: 0.25;
    }
    72% {
        transform: none;
        opacity: 0.25;
    }
    75% {
        transform: translate(${Math.floor(Math.random() * 6) + 1}px, ${Math.floor(Math.random() * 6) + 1}px);
        opacity: 0.5;
    }
    80% {
        transform: none;
        opacity: 0.25;
    }
    100% {
        transform: none;
        opacity: 0.25;
    }
`;
