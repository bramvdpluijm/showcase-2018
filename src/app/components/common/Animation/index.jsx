import React from 'react';
import PT from 'prop-types';
import styled from 'styled-components';
import isNode from 'detect-node';
import { Glitch1, Glitch2, Glitch3 } from './animation';
import Logo from 'vectors/bram-logo-white.svg';

const GlitchHolder = styled.div`
    display: flex;
    justify-content: center;
    width: 50%;
    svg {
        position: absolute;
        max-width: 450px!important;
        height: 100%;
        top: 0;
        animation: ${Glitch1} 2.5s infinite;
        &:nth-of-type(2) {
            stroke: #67f3da;
            animation: ${Glitch2} 2.5s infinite;
        }
        &:nth-of-type(3) {
            stroke: #f16f6f;
            animation: ${Glitch3} 2.5s infinite;
        }
    }
`;

class Animation extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <GlitchHolder>
                <Logo />
                <Logo />
                <Logo />
            </GlitchHolder>
        )
    }
}

Animation.propTypes = {
    children: PT.object,
    path: PT.string,
    loop: PT.bool,
    yoyo: PT.bool,
    autoPlay: PT.bool,
};

export default Animation;
