import React, { Fragment } from 'react';
import 'rc-slider/assets/index.css';
import PT from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import device from 'services/device';
import { setSlideIndex } from 'ducks/ui';

import Slider from 'rc-slider';
import Carousel from 'nuka-carousel';
import styled from 'styled-components';

const { Handle } = Slider;

const CustomRangeSlider = styled(Slider)`
    width: 75%!important;
    position: absolute!important;
    bottom: 60px;
    left: 50%;
    transform: translateX(-50%);
    @media ${device.desktop} {
        width: 48%!important;
    }
    & > div:nth-of-type(4) {
        transition: ${props => props.removeTransition ? 'none' : 'left 0.3s ease-in-out'}!important;
    }
    & > div {
        &:nth-of-type(1) {
            height: 2px!important;
        }
        &:nth-of-type(2) {
            height: 2px!important;
        }
        &:nth-of-type(3) {
            background-color: #656565;
            & > span {
                background-color: #656565;
                border-color: #656565;
            }
        }
    }
`;

const CustomCarousel = styled(Carousel)`
    position: absolute!important;
    top: 50%;
    transform: translateY(-50%);
    & > div {
        transition: transform 0.3s ease-in-out;
        transform: scale(${props => props.zoomOut ? '0.8' : '1'})!important;
        & ul {
            & li {
                transition: all 0.3s ease-in-out;
            }
        }
    }
`;

const CustomHandle = styled(Handle)`
    border: none!important;
    height: 30px!important;
    width: 30px!important;
    border: 2px solid #fff!important;
    display: flex;
    font-family: 'Lato';
    font-size: 12px;
    align-items: center;
    justify-content: center;
    background-color: #000!important;
    margin-top: -14px!important;
    color: #fff;
`;

class CarouselSlider extends React.Component {
    constructor(props) {
        super();
        this.state = {
            slideIndex: props.reduxSlideIndex,
            slideCount: props.slideCount,
            cellSpacing: __CLIENT__ ? window.innerWidth / 70 : 0,
            zoomOut: false,
            removeTransition: false,
        };
    }

    componentWillMount() {
        const marksObject = {};
        const { slideCount } = this.state;
        for (let i = 0; i < slideCount; i++) {
            marksObject[i] = '';
        }
        this.setState({ marks: marksObject });
    }

    componentDidMount() {
        if (__CLIENT__) {
            window.addEventListener('keydown', this.measureArrows);
        }
    }

    componentWillUnmount() {
        if (__CLIENT__) {
            window.removeEventListener('keydown', this.measureArrows);
        }
    }

    onBeforeChange = () => {
        const { cellSpacing } = this.state;
        this.setState({
            zoomOut: true,
            cellSpacing: cellSpacing * -5,
            removeTransition: true,
        });
    }

    onChange = (props) => {
        this.setState({ slideIndex: props });
        this.props.setSlideIndex(props);
    }

    onAfterChange = () => {
        const { slideIndex } = this.state;
        this.setState({
            zoomOut: false,
            cellSpacing: __CLIENT__ ? window.innerWidth / 70 : 0,
            slideIndex: Math.round(slideIndex),
            removeTransition: false,
        });
        this.props.setSlideIndex(Math.round(slideIndex));
    }

    measureArrows = (e) => {
        if (e.key === 'ArrowLeft') {
            this.progressBack();
        }
        if (e.key === 'ArrowRight') {
            this.progressForward();
        }
    }

    progressBack = () => {
        const { slideIndex } = this.state;
        if (!slideIndex - 1 < 0) {
            this.setState({ slideIndex: slideIndex - 1 });
            this.props.setSlideIndex(slideIndex - 1);
        }
    }

    progressForward = () => {
        const { slideIndex, slideCount } = this.state;
        if (slideIndex + 1 !== slideCount) {
            this.setState({ slideIndex: slideIndex + 1 });
            this.props.setSlideIndex(slideIndex + 1);
        }
    }

    handle = (props) => {
        const { value, dragging, index, ...restProps } = props;
        return (
            <CustomHandle value={Math.round(value) + 1} {...restProps}>{Math.round(value) + 1}</CustomHandle>
        );
    };

    afterSlide = (slideIndex) => {
        this.setState({ slideIndex });
        this.props.setSlideIndex(slideIndex);
    }

    render() {
        const { children } = this.props;
        const { slideIndex, cellSpacing, zoomOut, slideCount, removeTransition, marks } = this.state;

        const childWithProp = React.Children.map(children, child => (
            React.cloneElement(child, { slideIndex })
        ));

        return (
            <Fragment>
                <CustomCarousel
                    slideWidth={(__CLIENT__ && window.innerWidth > 768) ? 0.5714286 : 0.8}
                    cellAlign="center"
                    cellSpacing={cellSpacing}
                    afterSlide={slideIndex => this.afterSlide(slideIndex)}
                    slideIndex={slideIndex}
                    renderBottomCenterControls={() => {}}
                    renderCenterLeftControls={() => {}}
                    renderCenterRightControls={() => {}}
                    zoomOut={zoomOut}
                >
                    {childWithProp}
                </CustomCarousel>

                <CustomRangeSlider
                    min={0}
                    max={slideCount - 1}
                    defaultValue={0}
                    onBeforeChange={this.onBeforeChange}
                    onChange={this.onChange}
                    onAfterChange={this.onAfterChange}
                    handle={this.handle}
                    value={slideIndex}
                    step={0.001}
                    removeTransition={removeTransition}
                    marks={marks}
                />
            </Fragment>
        );
    }
}

CarouselSlider.propTypes = {
    value: PT.number,
    dragging: PT.bool,
    index: PT.number,
    children: PT.array,
    slideCount: PT.number,
    reduxSlideIndex: PT.number,
};

const mapDispatchToProps = dispatch => ({
    setSlideIndex: slideIndex => (
        dispatch(setSlideIndex(slideIndex))
    ),
});

export default compose(
    connect(state => ({
        reduxSlideIndex: state.ui.slideIndex,
    }), mapDispatchToProps),
)(CarouselSlider);
