import React from 'react';
import PT from 'prop-types';
import { Route } from 'react-router-dom';
import styled from 'styled-components';
import posed, { PoseGroup } from 'react-pose';
import { tween, easing } from 'popmotion';
import ReactGA from 'react-ga';

const PageContent = styled(posed.div({
    enter: {
        opacity: 1,
        transition: props => tween({
            ...props,
            duration: 300,
            ease: easing.easeInOut,
        }),
    },
    exit: {
        opacity: 0,
        transition: props => tween({
            ...props,
            duration: 300,
            ease: easing.easeInOut,
        }),
    },
}))`
    min-height: ${__CLIENT__ && `${window.innerHeight}px`};
    background-color: ${props => props.light ? '#f9f9f9' : '#171717'};
`;

class FadeRoute extends React.Component {
    componentDidMount() {
        if (__CLIENT__) {
            ReactGA.initialize('UA-107808924-3');
            ReactGA.pageview(window.location.pathname + window.location.search);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location !== nextProps.location) {
            if (__CLIENT__) {
                ReactGA.pageview(window.location.pathname + window.location.search);
            }
        }
    }

    render() {
        const { path, component, exact, lightTheme } = this.props;
        const Component = component;
        return (
            <PoseGroup>
                <PageContent light={lightTheme} key={path}>
                    <Route
                        path={path}
                        exact={exact}
                        component={props => <Component {...props} light={lightTheme} />}
                    />
                </PageContent>
            </PoseGroup>
        )
    }
};

FadeRoute.propTypes = {
    path: PT.string,
    component: PT.func,
    exact: PT.bool,
    lightTheme: PT.bool,
};

export default FadeRoute;
