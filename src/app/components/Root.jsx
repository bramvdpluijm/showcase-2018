import React from 'react';
import store from 'app/store';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import globalStyling from 'styles';
import theme from 'styles/theme';
import 'favicons/android-chrome-192x192.png?external';
import 'favicons/android-chrome-512x512.png?external';
import 'favicons/apple-touch-icon.png?external';
import 'favicons/browserconfig.xml?external';
import 'favicons/favicon-16x16.png?external';
import 'favicons/favicon-32x32.png?external';
import 'favicons/favicon.ico?external';
import 'favicons/mstile-150x150.png?external';
import 'favicons/safari-pinned-tab.svg?external';
import 'favicons/site.webmanifest';
import App from './App';


globalStyling();

const Root = () => (
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </ThemeProvider>
    </Provider>
);

export default Root;
