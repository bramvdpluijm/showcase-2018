import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Landing from 'modules/Landing';
import Projects from 'modules/Projects';
import ProjectDetail from 'modules/ProjectDetail';
import About from 'modules/About';
import Blog from 'modules/Blog';
// import BlogDetail from 'modules/BlogDetail';
import NotFound from 'modules/NotFound';
import FadeRoute from 'common/FadeRoute';

class App extends React.Component {
    render() {
        return (
            <main>
                <Switch>
                    <FadeRoute path="/" component={Landing} exact />
                    <FadeRoute path="/projecten" component={Projects} />
                    <FadeRoute path="/over-mij" component={About} />
                    <FadeRoute path="/project/:projectName?" component={ProjectDetail} />
                    <FadeRoute path="/blog" component={Blog} lightTheme />
                    {/* <FadeRoute path="/blog/:postSlug?" component={BlogDetail} /> */}
                    {/* <FadeRoute path="/404" component={NotFound} /> */}
                    {/* <Redirect to="/404" /> */}
                </Switch>
            </main>
        )
    }
};

export default App;
