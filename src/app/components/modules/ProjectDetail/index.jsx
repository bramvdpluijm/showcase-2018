import React, { Component, Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import styled, { injectGlobal } from 'styled-components';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PT from 'prop-types';
import { Helmet } from 'react-helmet';
import LazyLoad from 'react-lazyload';
import Menu from 'common/Menu';
import Database from 'data/database.js';
import createUrlTitle from 'services/createUrlTitle';

import Logo from 'vectors/bram-logo-white.svg';
import BackArrow from 'vectors/back-arrow.svg';

import history from 'services/history';
import device from 'services/device';

import { fetchProjectDetail } from 'ducks/projects';

const ProjectContainer = styled.div`
    max-width: 1440px;
    margin: 0px auto;
    padding-bottom: 50px;
    .visit-website {
        position: relative;
        display: block;
        width: 100vw;
        left: calc(-50vw + 50%);
        right: 0;
        background-color: #b9a680;
        text-align: center;
        text-decoration: none;
        color: #fff;
        font-size: 18px;
        padding: 20px 40px;
        letter-spacing: 2px;
        @media ${device.desktop} {
            padding: 40px 80px;
            font-size: 29px;
        }
    }
`;

const ProjectHeader = styled.header`
    display: block;
    width: 100%;
    padding: 30px;
    color: #fff;
    h1 {
        display: block;
        max-width: 1120px;
        font-size: calc(29px + 89 * ((100vw - 320px) / 1360));
        margin: 0px auto;
        padding: 80px 20px 40px;
        text-align: center;
        @media ${device.desktopL} {
            font-size: 118px;
        }
    }
`;

export const ProjectImage = styled.div`
    position: relative;
    img {
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
    }
    & > div {
        &:before {
            content: '';
            display: block;
            padding-top: 56.25%;
            background-color: ${props => props.averageColor};
        }
    }
`;

const ProjectText = styled.div`
    font-size: 18px;
    line-height: 1.6;
    color: #999;
    width: 100%;
    max-width: 1680px;
    padding: 80px 20px 40px;
    margin-left: auto;
    margin-right: auto;

    @media ${device.tablet} {
        font-size: 18px;
        width: 66.6666666667%;
        max-width: 1120px;
    }
    @media ${device.desktop} {
        font-size: 20px;
        width: 50%;
        max-width: 840px;
    }
    @media ${device.desktopL} {
        font-size: 23.4px;
    }
`;

export const MenuTopbar = styled.div`
    top: 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
    z-index: 4;
    width: 100%;
    padding: 30px;
    svg {
        width: auto;
        height: 70px;
    }
`;

const Macbook = styled.div`
    width: 100%;
    max-width: 1440px;
    figure {
        width: 100%;
        height: 0;
        padding-bottom: 62.5%;
        overflow: hidden;
        position: relative;
        margin: 0;
        img {
            position: absolute;
            width: 100%;
            left: 0;
            top: 0;
            right: 0;
        }
    }
`;

const MacbookDisplay = styled.div`
    @media ${device.desktop} {
        position: relative;
        width: 86%;
        margin: 0 auto;
        border: 1px solid #333;
        padding: 64px 40px 40px;
        border-top-left-radius: 40px;
        border-top-right-radius: 40px;

        &:before {
            content: "";
            display: block;
            position: absolute;
            top: 32px;
            width: 13px;
            height: 13px;
            left: 50%;
            -webkit-transform: translate(-50%);
            transform: translate(-50%);
            border-radius: 100%;
            border: 1px solid #333;
        }
    }
`;

const MacbookBody = styled.div`
    display: none;
    @media ${device.desktop} {
        display: block;
        width: 100%;
        border: 1px solid #333;
        position: relative;
        top: -1px;
        height: 51px;
        border-bottom-left-radius: 48px 32px;
        border-bottom-right-radius: 48px 32px;
        &:before {
            content: "";
            position: absolute;
            width: 15%;
            top: -1px;
            left: 50%;
            -webkit-transform: translate(-50%);
            transform: translate(-50%);
            background: transparent;
            border: 1px solid #333;
            height: 24px;
            border-bottom-left-radius: 32px 21px;
            border-bottom-right-radius: 32px 21px;
        }
    }
`;

const WhatDidIDo = styled.div`
    width: 100%;
    border-top: 1px solid #1c1c1c;
    border-bottom: 1px solid #1c1c1c;
    padding: 40px 20px;
    position: relative;
    margin: 40px auto 120px;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;

    & > div:first-of-type {
        width: 100%;
        position: absolute;
        top: 0;
        left: 50%;
        transform: translate3d(-50%,-50%,0);
        color: #c0a062;
        text-transform: uppercase;
        font-size: 18px;
        margin-bottom: 40px;
        letter-spacing: 2px;
        font-weight: 400;
        width: 100%;
        text-align: center;
    }

    & div > span:first-of-type {
        padding: 0 40px;
        background-color: #171717;
        color: #b9a680;
        letter-spacing: 2px;
        font-size: 18px;
        font-family: 'Merriweather',sans-serif;
    }

    @media ${device.tablet} {
        width: 80%;
    }
    @media ${device.desktop} {
        width: 60%;
    }
    @media ${device.desktopL} {
        width: 60%;
    }
`;

const Skill = styled.span`
    color: #999;
    font-size: 18px;
    text-transform: uppercase;
    letter-spacing: 2px;
    padding: 10px;
    text-align: center;
`;

const GoBack = styled.div`
    position: absolute;
    top: 130px;
    left: 30px;
    display: flex;
    align-items: center;
    cursor: pointer;
    & > div {
        width: 60px;
        height: 60px;
        background-color: #b9a680;
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        align-items: center;
        svg {
            height: 20px;
            transition: all 0.3s ease-in-out;
        }
        @media ${device.desktop} {
            width: 80px;
            height: 80px;
            svg {
                height: 25px;
            }
        }
    }
    span {
        opacity: 0;
        color: #999;
        margin-left: 20px;
        transition: all 0.3s ease-in-out;
    }
    &:hover {
        span {
            opacity: 1;
            transition: all 0.3s ease-in-out;
        }
        & > div {
            svg {
                margin-left: -10px;
                transition: all 0.3s ease-in-out;
            }
        }
    }
`;

const ProjectImages = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    padding-top: 50px;
    padding-left: 30px;
    padding-right: 30px;
    max-width: 1280px;
    margin: 0px auto;
    img {
        margin-bottom: 40px;
        width: 100%;
        height: 100%;
    }
    @media ${device.desktop} {
        padding-left: 0px;
        padding-right: 0px;
    }
`;

class ProjectDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectName: props.match.params.projectName,
        };
    }

    componentWillMount() {
        injectGlobal`
            body {
                background-color: #171717;
            }
        `;
    }

    componentDidMount() {
        const { projectName } = this.state;
        const { fetchProjectDetail } = this.props;

        fetchProjectDetail(projectName);
    }

    render() {
        const { projectDetails, loadingProjectDetail } = this.props;
        const { projectName } = this.state;

        if (Database.projects.filter(project => (projectName === createUrlTitle(project.project_name))).length === 0) {
            return (
                <Redirect to="/404" />
            );
        }

        return (
            <Fragment>
                <MenuTopbar>
                    <Logo />
                    <Menu />
                </MenuTopbar>
                {!loadingProjectDetail && projectDetails.length && (
                    <ProjectContainer>
                        <Helmet>
                            <title>{projectDetails[0].project_name} | Bram van der Pluijm</title>
                            <meta name="robots" content="index, follow" />
                            <meta name="description" content={`De project detailpagina van het project ${projectDetails[0].project_name} van Bram van der Pluijm.`} />
                            <meta name="keywords" content={projectDetails[0].project_keywords} />
                        </Helmet>
                        <GoBack onClick={history.goBack}>
                            <div>
                                <BackArrow />
                            </div>
                            <span>Terug naar projecten</span>
                        </GoBack>

                        <ProjectHeader>
                            <h1>{projectDetails[0].project_name}</h1>
                        </ProjectHeader>

                        <Macbook>
                            <MacbookDisplay>
                                <figure>
                                    <img src={projectDetails[0].project_image} alt={projectDetails[0].project_name} />
                                </figure>
                            </MacbookDisplay>
                            <MacbookBody />
                        </Macbook>

                        <ProjectText>
                            {projectDetails[0].project_alineas.map(alinea => (
                                typeof alinea.titel !== 'undefined' ? (
                                    <div>
                                        <span>{alinea.titel}</span>
                                        <p>{alinea.text}</p>
                                    </div>
                                ) : (
                                    <div>
                                        <p>{alinea.text}</p>
                                    </div>
                                )
                            ))}
                        </ProjectText>

                        <WhatDidIDo>
                            <div>
                                <span>WAT IK HEB GEDAAN</span>
                            </div>
                            {projectDetails[0].skills.map(skill => (
                                <Skill>
                                    <span>#</span>
                                    {skill}
                                </Skill>
                            ))}
                        </WhatDidIDo>
                        {typeof projectDetails[0].project_url !== 'undefined' && (
                            <a className="visit-website" rel="noopener noreferrer" target="_blank" href={projectDetails[0].project_url}>GA NAAR WEBSITE</a>
                        )}

                        {typeof projectDetails[0].project_screencaps !== 'undefined' && (
                            <ProjectImages>
                                {projectDetails[0].project_screencaps.map(screencap => (
                                    <LazyLoad key={screencap} height={250}>
                                        <img src={screencap} />
                                    </LazyLoad>
                                ))}
                            </ProjectImages>
                        )}
                    </ProjectContainer>
                )}
            </Fragment>
        );
    }
}

ProjectDetail.propTypes = {
    fetchProjectDetail: PT.func,
    projectName: PT.string,
    projectDetails: PT.array,
    loadingProjectDetail: PT.bool,
};

export default compose(
    connect(state => ({
        loadingProjectDetail: state.projects.loadingProjectDetail,
        projectDetails: state.projects.projectDetails,
    }), { fetchProjectDetail }),
)(ProjectDetail);
