import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import Menu from 'common/Menu';
import Logo from 'vectors/bram-logo-white.svg';

import device from 'services/device';

const CenteredSection = styled.section`
    display: flex;
    align-items: center;
    justify-content: center;
    height: ${__CLIENT__ && `${window.innerHeight}px`};
    background-color: #171717;
`;

const MenuTopbar = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: fixed;
    z-index: 4;
    width: 100%;
    padding: 30px;
    svg {
        width: auto;
        height: 70px;
    }
`;

const NotFoundText = styled.div`
    h1 {
        color: #fff;
        font-size: 100px;
        text-align: center;
        margin: 0;
        margin-bottom: 30px;
        @media ${device.desktopL} {
            font-size: 130px;
        }
    }
    span {
        color: #999;
        @media ${device.tablet} {
            font-size: 18px;
        }
        @media ${device.desktop} {
            font-size: 20px;
        }
        @media ${device.desktopL} {
            font-size: 23.4px;
        }
    }
`;

class NotFound extends React.Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Oeps.. pagina kwijt? | Bram van der Pluijm</title>
                    <meta name="robots" content="index, follow" />
                    <meta name="description" content="Deze pagina werd niet gevonden. Probeer opnieuw." />
                    <meta name="keywords" content="Uh oh, not found, lost, oeps.. pagina kwijt, 404, error" />
                </Helmet>
                <MenuTopbar>
                    <Logo />
                    <Menu />
                </MenuTopbar>
                <CenteredSection>
                    <NotFoundText>
                        <h1>404</h1>
                        <span>Oeps.. we konden die pagina niet vinden</span>
                    </NotFoundText>
                </CenteredSection>
            </Fragment>
        )
    }
}

export default NotFound;
