import React, { Fragment } from 'react';
import Menu from 'common/Menu';
import styled, { injectGlobal } from 'styled-components';
import { connect } from 'react-redux';
import posed from 'react-pose';
import { tween, easing } from 'popmotion';
import { compose } from 'redux';
import PT from 'prop-types';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Database from 'data/database.js';

import device from 'services/device';
import createUrlTitle from 'services/createUrlTitle';

import Logo from 'vectors/bram-logo-white.svg';
import CarouselSlider from 'common/CarouselSlider';

const SlideContainer = styled.div`
    display: flex;
    align-items: center;
`;

const Slide = styled.div`
    position: relative;
    height: 45vw;
    width: 100%;
    background-image: ${props => `url(${props.background})`};
    background-size: cover;
    background-position: center center;
    transform: scale(${props => props.zoomIn ? '1' : '0.7'});
    transition: transform 0.3s ease-in-out;
    @media ${device.desktop} {
        height: 33.333vw;
    }

    &:after {
        content: '';
        display: block;
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0, 0.5);
    }
`;

const SlideTitle = styled.div`
    position: absolute;
    max-width: 450px;
    top: 50%;
    left: -5%;
    transform: translateY(-50%);
    z-index: 1;
    font-family: 'Merriweather',sans-serif;
    @media ${device.desktop} {
        left: -10%;
    }

`;

const SlideTitleLine = styled.div`
    display: block;
    overflow: hidden;
`;

const SlideTitleInner = styled(posed.div({
    in: {
        y: '0%',
        transition: props => tween({
            ...props,
            duration: 300,
            ease: easing.easeInOut,
        }),
    },
    out: {
        y: '120%',
        transition: props => tween({
            ...props,
            duration: 300,
            ease: easing.easeInOut,
        }),
    },
}))`
    position: relative;
    display: block;
    color: #fff;
    font-size: 35px;

    @media ${device.desktop} {
        font-size: 60px;
    }
`;

export const MenuTopbar = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: fixed;
    z-index: 4;
    width: 100%;
    padding: 30px;
    svg {
        width: auto;
        height: 70px;
    }
`;

class Projects extends React.Component {
    componentWillMount() {
        injectGlobal`
            body {
                background-color: #171717;
            }
        `;
    }

    render() {
        const { slideIndex } = this.props;
        return (
            <Fragment>
                <Helmet>
                    <title>Projecten | Bram van der Pluijm</title>
                    <meta name="robots" content="index, follow" />
                    <meta name="description" content="Overzicht van alle projecten van Bram van der Pluijm." />
                    <meta name="keywords" content="projecten, project, overzicht, apollo hotels, thermae, huisblusser.nl, huisblusser, natuurlijkpresteren, natuurlijk presteren, verscentrum nederland, vcn, nocknock" />
                </Helmet>
                <MenuTopbar>
                    <Logo />
                    <Menu />
                </MenuTopbar>
                <CarouselSlider slideCount={Database.projects.length}>
                    {Database.projects.map((project, index) => (
                        <Link to={`/project/${createUrlTitle(project.project_name)}`} key={project.project_name}>
                            <SlideContainer>
                                <Slide zoomIn={index === slideIndex} background={project.project_image}>
                                    <SlideTitle>
                                        {project.project_lines.map(line => (
                                            <SlideTitleLine key={line}>
                                                <SlideTitleInner pose={index === slideIndex ? 'in' : 'out'}>
                                                    {line}
                                                </SlideTitleInner>
                                            </SlideTitleLine>
                                        ))}
                                    </SlideTitle>
                                </Slide>
                            </SlideContainer>
                        </Link>
                    ))}
                </CarouselSlider>
            </Fragment>
        );
    }
}

Projects.propTypes = { slideIndex: PT.number };

export default compose(
    connect(state => ({ slideIndex: state.ui.slideIndex })),
)(Projects);
