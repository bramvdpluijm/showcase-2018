import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Animation from 'common/Animation';
import Logo from 'vectors/bram-logo-white.svg';
import device from 'services/device';

const AnimationHolder = styled.div`
    display: flex;
    justify-content: center;
    width: 50%;
    svg {
        width: 80%;
        max-width: 500px;
        height: 100%;
        @media ${device.desktop} {
            width: 100%;
        }
    }
    .custom-button {
        position: absolute;
        bottom: 60px;
        text-decoration: none;
        color: #fff;
        padding: 12px 30px 15px;
        border-radius: 30px;
        border: 2px solid #fff;
        @media ${device.desktop} {
            color: #525252;
            border: 2px solid #525252;
            transition: all 0.3s ease-in-out;
            &:hover {
                color: #fff;
                border-color: #fff;
                transition: all 0.3s ease-in-out;
            }
        }
    }
`;

const LandingAnimation = () => (
    <AnimationHolder>
        <Animation>
            <Logo />
        </Animation>
        <Link to="/projecten" className="custom-button">Ga naar projecten</Link>
    </AnimationHolder>
);

export default LandingAnimation;
