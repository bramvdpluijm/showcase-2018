import React from 'react';
import styled, { injectGlobal } from 'styled-components';
import { Helmet } from 'react-helmet';
import Menu from 'common/Menu';
import LandingAnimation from './components/LandingAnimation';

const CenteredSection = styled.section`
    display: flex;
    align-items: center;
    justify-content: center;
    height: ${__CLIENT__ && `${window.innerHeight}px`};
    background-color: #171717;
`;

class Landing extends React.Component {
    componentWillMount() {
        injectGlobal`
            body {
                background-color: #171717;
            }
        `;
    }

    render() {
        return (
            <CenteredSection>
                <Helmet>
                    <title>Showcase | Bram van der Pluijm</title>
                    <meta name="robots" content="index, follow" />
                    <meta name="description" content="Showcase website van Bram van der Pluijm, React Frontend Developer." />
                    <meta name="keywords" content="showcase, website, portfolio, react, redux, frontend, front-end, Bram, Bram van der Pluijm, bram van der pluijm, bram vd pluijm, pluijm, pluijm.io, bram, van der pluijm" />
                </Helmet>
                <Menu />
                <LandingAnimation />
            </CenteredSection>
        );
    }
}

export default Landing;
