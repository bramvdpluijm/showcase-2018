import React, { Component, Fragment } from 'react';
import styled, { injectGlobal } from 'styled-components';
import moment from 'moment';
import PT from 'prop-types';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';

import { fetchBlogData, fetchCategories } from 'ducks/blog';
import device from 'services/device';

import Logo from 'vectors/bram-logo-black.svg';
import Menu from 'common/Menu';
import Loader from 'common/Loader';

const CenteredSection = styled.section`
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #f9f9f9;
    padding-top: 200px;
    padding-bottom: 60px;
    @media ${device.desktop} {
        padding-top: 120px;
    }
`;

const MenuTopbar = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: fixed;
    z-index: 4;
    width: 100%;
    padding: 30px;
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+0,ffffff+100&1+0,0+100 */
    background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(255,255,255,0) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */
    svg {
        width: auto;
        height: 70px;
    }
`;

const BlogList = styled.div`
    display: flex;
    width: 100%;
    max-width: 1680px;
    padding: 0px 20px;
    flex-flow: row wrap;
    align-items: stretch;
    .blog-title {
        flex-basis: 100%;
        text-align: center;
        padding: 0px 20px;
        color: #212121;
        font-size: calc(29px + 55 * ((100vw - 320px) / 1360));
    }
`;

const BlogItem = styled.div`
    display: flex;
    flex-flow: row wrap;
    flex: 0 0 100%;
    padding: 20px;
    padding-bottom: 0px;
    align-items: stretch;
    @media ${device.tablet} {
        flex: 0 0 50%;
    }

    @media ${device.desktop} {
        flex: 0 0 33.333%;
    }
    a {
        display: flex;
        flex-flow: row wrap;
        flex: 1 1 100%;
        background-color: #fff;
        text-decoration: none;
        color: #000;
        height: 100%;
        box-shadow: 0 1px 1px #e6e6e6;
        transition: all .25s ease-out;
        padding: 50px 40px 60px;
        @media ${device.tablet} {
            padding: 60px 45px;
        }
        @media ${device.desktop} {
            padding: 60px 60px;
        }
        .post-information {
            position: relative;
            align-self: flex-end;
            &:before {
                content: '';
                display: block;
                position: absolute;
                left: 0;
                top: 0;
                height: 1px;
                width: 30%;
                background-color: #e0b868;
                transition: all .25s ease-out;
            }
            .info {
                padding-top: 20px;
            }
        }
    }
    h3 {
        flex-basis: 100%;
        margin-top: 0;
        margin-bottom: 40px;
        @media ${device.desktop} {
            font-size: 29px;
        }
    }
    .category {
        display: block;
        flex-basis: 100%;
        color: #999;
    }
    .date {
        display: block;
        color: #b9a680;
    }
    &:hover {
        a {
            box-shadow: 0 10px 25px #e6e6e6;
            transform: translateY(-2px);
            transition: all .25s ease-out;
            .post-information {
                &:before {
                    width: 50%;
                    transition: all .25s ease-out;
                }
            }
        }
    }
`;

class BlogOverview extends Component {
    componentWillMount() {
        this.props.fetchCategories();
        this.props.fetchBlogData();

        injectGlobal`
            body {
                background-color: #f9f9f9;
            }
        `;
    }

    getPostCategorySlug(post) {
        const postCategory = post.categories[0];
        const { categories } = this.props;
        return categories.filter(category => (category.id === postCategory))[0].slug;
    }

    render() {
        const { blogData, loadingBlogData, loadingCategories, categories } = this.props;
        return (
            <Fragment>
                <Helmet>
                    <title>Blog | Bram van der Pluijm</title>
                    <meta name="robots" content="index, follow" />
                    <meta name="description" content="Blog pagina van Bram van der Pluijm." />
                    <meta name="keywords" content="blog, weblog, brewing, beer, projects" />
                </Helmet>
                <MenuTopbar>
                    <Logo />
                    <Menu black />
                </MenuTopbar>
                <CenteredSection>
                    <BlogList>
                        <h2 className="blog-title">Mijn Blog</h2>

                        {(loadingCategories || loadingBlogData || blogData.length === 0) && (
                            <Loader />
                        )}

                        {(!loadingCategories && !loadingBlogData && blogData.length > 0) && blogData.map(post => (
                            <BlogItem key={post.id}>
                                <Link to={`/blog/${this.getPostCategorySlug(post)}/${post.slug}`}>
                                    <h3>{post.title.rendered}</h3>

                                    <div className="post-information">
                                        <div className="info">
                                            <span className="category">
                                                {post.categories.length > 1 ? 'Categorieen: ' : 'Categorie: '}
                                                {post.categories.map(postcategory => (
                                                    categories.map((category) => {
                                                        if (category.id === postcategory) {
                                                            return category.name;
                                                        }
                                                    })
                                                ))}
                                            </span>
                                            <span className="date">{moment(post.date).fromNow()}</span>
                                        </div>
                                    </div>
                                </Link>
                            </BlogItem>
                        ))}
                    </BlogList>
                </CenteredSection>
            </Fragment>
        );
    }
}

BlogOverview.propTypes = {
    categories: PT.array,
};

export default compose(
    connect(state => ({
        blogData: state.blog.blogData,
        loadingBlogData: state.blog.loadingBlogData,
        categories: state.blog.categories,
        loadingCategories: state.blog.loadingCategories,
    }), { fetchBlogData, fetchCategories }),
)(BlogOverview);
