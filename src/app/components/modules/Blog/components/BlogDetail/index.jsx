import React, { Component, Fragment } from 'react';
import styled, { injectGlobal } from 'styled-components';
import { Helmet } from 'react-helmet';
import { compose } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import PT from 'prop-types';

import Menu from 'common/Menu';
import Loader from 'common/Loader';

import Logo from 'vectors/bram-logo-black.svg';
import BackArrow from 'vectors/back-arrow.svg';

import history from 'services/history';
import device from 'services/device';

import { fetchBlogDetail, fetchUsers } from 'ducks/blog';

export const MenuTopbar = styled.div`
    top: 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
    z-index: 4;
    width: 100%;
    padding: 30px;
    svg {
        width: auto;
        height: 70px;
    }
`;

const ProjectContainer = styled.div`
    max-width: 1440px;
    margin: 0px auto;
    padding-bottom: 50px;
`;

const GoBack = styled.div`
    position: absolute;
    top: 130px;
    left: 30px;
    display: flex;
    align-items: center;
    cursor: pointer;
    & > div {
        width: 60px;
        height: 60px;
        background-color: #b9a680;
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        align-items: center;
        svg {
            height: 20px;
            transition: all 0.3s ease-in-out;
        }
        @media ${device.desktop} {
            width: 80px;
            height: 80px;
            svg {
                height: 25px;
            }
        }
    }
    span {
        opacity: 0;
        color: #999;
        margin-left: 20px;
        transition: all 0.3s ease-in-out;
    }
    &:hover {
        span {
            opacity: 1;
            transition: all 0.3s ease-in-out;
        }
        & > div {
            svg {
                margin-left: -10px;
                transition: all 0.3s ease-in-out;
            }
        }
    }
`;

const ProjectHeader = styled.header`
    display: block;
    width: 100%;
    padding: 80px 30px 40px;
    margin: 0px auto;
    color: #fff;
    h1 {
        display: block;
        font-size: calc(29px + 60 * ((100vw - 320px) / 1360));
        text-align: left;
        color: #212121;
        margin-bottom: 0px;
        span {
            font-size: 18px;
            display: block;
            color: #b9a680;
            margin-bottom: 20px;
        }
        @media ${device.desktopL} {
            font-size: 84px;
        }
    }
    @media ${device.tablet} {
        padding: 80px 20px 40px;
        max-width: 650px;
    }
    @media ${device.desktop} {
        padding: 80px 20px 40px;
        max-width: 840px;
    }
`;

const BlogText = styled.div`
    font-size: 18px;
    line-height: 1.7;
    color: #999;
    width: 100%;
    max-width: 1120px;
    padding: 40px 30px 40px;
    margin-left: auto;
    margin-right: auto;

    @media ${device.tablet} {
        padding: 40px 20px 40px;
        font-size: 18px;
        max-width: 650px;
    }
    @media ${device.desktop} {
        padding: 40px 20px 40px;
        max-width: 840px;
        font-size: 20px;
    }
    @media ${device.desktopL} {
        padding: 40px 20px 40px;
        font-size: 23.4px;
    }

    > p {
        &:first-of-type {
            &::first-letter {
                float: left;
                line-height: 1;
                font-size: 300%;
                margin: -20px 5px 0 0;
                font-weight: 700;
                color: #212121;
            }
        }
        b {
            font-family: 'LatoBold';
            color: #212121;
        }
    }

    p {
        img {
            width: 100%;
            height: auto;
        }
    }

    blockquote {
        font-size: 30px;
        position: relative;
        padding: 20px 0px;
        p {
            margin: 0px;
            font-style: italic;
            color: #777777;
        }
        &:before {
            content: '';
            height: 1px;
            background-color: #e0b868;
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 120px;
        }
        &:after {
            content: '';
            height: 1px;
            background-color: #e0b868;
            display: block;
            position: absolute;
            bottom: 0;
            left: 0;
            width: 120px;
        }
    }

    h1, h2, h3 {
        color: #212121;
    }
`;

class BlogDetail extends Component {
    componentWillMount() {
        this.props.fetchBlogDetail(this.props.match.params.postSlug);
        this.props.fetchUsers();

        injectGlobal`
            body {
                background-color: #f9f9f9;
            }
        `;
    }

    render() {
        const { blogDetail, loadingBlogDetail, loadingUsers } = this.props;
        return (
            <Fragment>
                <MenuTopbar>
                    <Logo />
                    <Menu black />
                </MenuTopbar>

                {(loadingBlogDetail || loadingUsers || blogDetail.length === 0) && (
                    <Loader />
                )}

                {!loadingBlogDetail && !loadingUsers && blogDetail.length > 0 && (
                    <ProjectContainer>
                        <Helmet>
                            <title>{`${blogDetail[0].title && blogDetail[0].title.rendered} | Bram van der Pluijm`}</title>
                            <meta name="robots" content="index, follow" />
                            <meta name="description" content={``} />
                            <meta name="keywords" content="" />
                        </Helmet>

                        <GoBack onClick={history.goBack}>
                            <div>
                                <BackArrow />
                            </div>
                            <span>Terug naar blog overzicht</span>
                        </GoBack>

                        <ProjectHeader>
                            <h1>
                                <span>
                                    {moment(blogDetail[0].date).fromNow()}
                                </span>
                                {blogDetail[0].title && blogDetail[0].title.rendered}
                            </h1>
                        </ProjectHeader>

                        <BlogText dangerouslySetInnerHTML={{ __html: blogDetail[0].content && blogDetail[0].content.rendered }} />
                    </ProjectContainer>
                )}
            </Fragment>
        );
    }
}

BlogDetail.propTypes = {
    fetchUsers: PT.func,
    blogDetail: PT.array,
    loadingBlogDetail: PT.bool,
    loadingUsers: PT.bool,
    fetchBlogDetail: PT.func,
};

const mapStateToProps = state => ({
    blogDetail: state.blog.blogDetail,
    loadingBlogDetail: state.blog.loadingBlogDetail,
    loadingUsers: state.blog.loadingUsers,
});

const mapDispatchToProps = dispatch => ({
    fetchBlogDetail: postSlug => (
        dispatch(fetchBlogDetail(postSlug))
    ),
    fetchUsers: () => (
        dispatch(fetchUsers())
    ),
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
)(BlogDetail);
