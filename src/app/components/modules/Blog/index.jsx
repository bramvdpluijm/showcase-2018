import React from 'react';
import { Route, Switch } from 'react-router-dom';
import moment from 'moment';
import PT from 'prop-types';

// import { NotFoundErrorMessage } from 'common/Errors';
import BlogOverview from './components/BlogOverview';
import BlogDetail from './components/BlogDetail';
import FadeRoute from 'common/FadeRoute';

moment.locale('nl');

const Index = (({ match }) => (
    <Switch>
        <FadeRoute exact path={`${match.url}`} component={BlogOverview} lightTheme />
        <FadeRoute exact path={`${match.url}/:postCategory/:postSlug`} component={BlogDetail} lightTheme />
        {/* <Route component={NotFoundErrorMessage} /> */}
    </Switch>
));

Index.propTypes = {
    match: PT.object,
};

export default Index;
