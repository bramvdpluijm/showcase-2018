import React, { Component, Fragment } from 'react';
import PT from 'prop-types';
import { Helmet } from 'react-helmet';
import { injectGlobal } from 'styled-components';

import Logo from 'vectors/bram-logo-white.svg';
import Menu from 'common/Menu';
import { CenteredSection, AboutMe, MenuTopbar } from './styled';

import Database from 'data/database.js';

class About extends Component {
    componentWillMount() {
        injectGlobal`
            body {
                background-color: #171717;
            }
        `;
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Over Mij | Bram van der Pluijm</title>
                    <meta name="robots" content="index, follow" />
                    <meta name="description" content="Over Mij pagina van Bram van der Pluijm. Wie ben ik en wat doe ik?" />
                    <meta name="keywords" content="over mij, about, about me, front-end, pixel, pixel perfect, label a, Label A, projecten, hello, bier" />
                </Helmet>
                <MenuTopbar>
                    <Logo />
                    <Menu />
                </MenuTopbar>
                <CenteredSection>
                    <AboutMe>
                        <Fragment>
                            <div>
                                <span>{Database.about.title}</span>
                                <h3>{Database.about.subtitle}</h3>
                            </div>
                            {Database.about.paragraphs && Database.about.paragraphs.map((paragraph, index) => (
                                <p key={index}>{paragraph}</p>
                            ))}
                        </Fragment>
                    </AboutMe>
                </CenteredSection>
            </Fragment>
        )
    }
}

About.propTypes = {
    about: PT.object,
    fetchAbout: PT.func,
    loadingAbout: PT.bool,
};

export default About;
