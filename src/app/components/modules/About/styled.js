import styled from 'styled-components';
import device from 'services/device';

export const CenteredSection = styled.section`
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #171717;
`;

export const AboutMe = styled.div`
    max-width: 800px;
    padding: 150px 30px 60px;
    span {
        color: #fff;
        font-size: 60px;
        font-family: 'Merriweather', sans-serif;
        @media ${device.desktop} {
            font-size: calc(29px + 89 * ((100vw - 320px) / 1360));
        }
        @media ${device.desktopL} {
            font-size: 118px;
        }
    }
    h3 {
        font-size: 45px;
        color: #fff;
        margin: 0px;
        color: #656565;
    }
    p {
        font-family: 'Lato';
        font-size: 18px;
        line-height: 1.6;
        color: #999;
        margin-top: 30px;
        @media ${device.tablet} {
            font-size: 18px;
        }
        @media ${device.desktop} {
            font-size: 20px;
        }
        @media ${device.desktopL} {
            font-size: 23.4px;
        }
    }
`;

export const MenuTopbar = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: fixed;
    z-index: 4;
    width: 100%;
    padding: 30px;
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#171717+0,171717+100&1+0,0+100 */
    background: -moz-linear-gradient(top, rgba(23,23,23,1) 0%, rgba(23,23,23,0) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(23,23,23,1) 0%,rgba(23,23,23,0) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(23,23,23,1) 0%,rgba(23,23,23,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#171717', endColorstr='#00171717',GradientType=0 ); /* IE6-9 */
    svg {
        width: auto;
        height: 70px;
    }
`;
