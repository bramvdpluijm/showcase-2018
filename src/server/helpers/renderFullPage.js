module.exports = ({ html = '', styleTags, helmet }) => (
    `
        <!DOCTYPE html>
        <html lang="nl">
            <head>
                <meta charset="utf-8">
                <base href="/">
                ${helmet.title.toString()}
                <link rel="shortcut icon" href="/static/favicon.ico" />
                <link rel="apple-touch-icon" sizes="180x180" href="/static/favicon/apple-touch-icon.png">
                <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon/favicon-16x16.png">
                <link rel="manifest" href="/static/favicon/site.webmanifest">
                <link rel="mask-icon" href="/static/favicon/safari-pinned-tab.svg" color="#000000">
                <meta name="msapplication-TileColor" content="#000000">
                <meta name="theme-color" content="#000000">
                ${styleTags || ''}
                ${html ? '<link rel="stylesheet" type="text/css" href="/style.css">' : ''}
                <meta name="apple-mobile-web-app-capable" content="yes">
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
                <meta name="google-site-verification" content="C0wXe0oiPSzLdZ5N9VGuA2kqKJbnC7qUc3jHdV6zjAI" />
                ${helmet.meta.toString()}
            </head>
            <body>
                <div id="app">${html}</div>
                <script src="/vendor.js"></script>
                <script src="/app.js"></script>
                <noscript>Uw browser ondersteunt geen JavaScript!</noscript>
            </body>
        </html>
    `
);
