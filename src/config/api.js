const getApiEndPoint = () => {
    switch (process.env.NODE_ENV) {
    case 'production':
        return 'https://cms.pluijm.io/wp-json/wp/v2';
    case 'acceptation':
        return 'https://cms.pluijm.io/wp-json/wp/v2';
    case 'test':
        return 'https://cms.pluijm.io/wp-json/wp/v2';
    default:
        return 'https://cms.pluijm.io/wp-json/wp/v2';
    }
};

export default getApiEndPoint();
